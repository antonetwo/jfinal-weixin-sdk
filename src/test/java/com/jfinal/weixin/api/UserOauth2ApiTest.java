package com.jfinal.weixin.api;

import com.alibaba.fastjson.JSON;
import com.jfinal.weixin.api.model.Follower;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-25
 */
public class UserOauth2ApiTest {

    private static String accessToken = "";
    private static String snsAccessToken = "";

    /**
     * 获取授权链接地址
     */
    @Test
    public void getSns_Base_url() throws UnsupportedEncodingException {
        String appId = "wx497d358fd6ad1c09";
        String redirectUrl = "http://2018.koo.be-xx.com/wx/oauth2/123";
//        String scope = "snsapi_base";
        String scope = "snsapi_userinfo";
        String state = "456";

        String url = UserOauth2Api.ice.getOauthURL(appId, URLEncoder.encode(redirectUrl, "UTF-8"), scope, state);
        System.out.println(url);
    }

    /**
     * 换取授权accessToken后取得用户资料
     * <p/>
     * 测试结果: 使用code换取的token只针对对应的粉丝有效, 实际授权后获取用户信息是以token作为标识,即一个用户一个token
     * 测试有效期内,openId不会产生作用,传入任意字符串都能查询对应openI的用户信息,实际情况openId 和 授权token一一对应
     */
    @Test
    public void findUserInfo() {
        String code = "011a223d77d2bd206a567bc740add62s";
        snsAccessToken = UserOauth2Api.ice.getSnsAccessToken("", "", code).getAccess_token();

        String openId = "ovpVQwncFN7kseFCcw-YVt9ie8C4";

        ApiResult result = UserOauth2Api.ice.getUserInfo(snsAccessToken, openId);

        if (result.isSuccessful()) {
            System.out.println(result.getJson());

            //转为对象
            Follower follower = UserOauth2Api.ice.convertFollower(result.getJson());
            if (follower != null) {
                System.out.println("得到对象:\n" + JSON.toJSONString(follower, true));
            }
        }

    }


}

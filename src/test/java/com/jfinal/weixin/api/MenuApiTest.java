package com.jfinal.weixin.api;

import com.alibaba.fastjson.JSON;
import com.jfinal.weixin.api.model.menu.Menu;
import com.jfinal.weixin.api.model.menu.MenuButton;
import com.jfinal.weixin.api.model.menu.MenuMatchrule;
import org.junit.Test;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-17
 */
public class MenuApiTest {

    @Test
    public void test() {
        System.out.println(WxTestKit.getAccessToken());
    }

    /**
     * 测试面向对象的自定义菜单转化
     * 01 只包含一级菜单
     */
    @Test
    public void menuOnlyFirst() {
        Menu menu = new Menu();

        MenuButton mb01 = new MenuButton();
        mb01.setType(MenuApi.MENU_TYPE_CLICK);
        mb01.setName("Click菜单");
        mb01.setKey("menu_click_1");

        MenuButton mb02 = new MenuButton();
        mb02.setType(MenuApi.MENU_TYPE_VIEW);
        mb02.setName("链接菜单");
        mb02.setUrl("http://www.baidu.com/");

        MenuButton mb03 = new MenuButton();
        mb03.setType(MenuApi.MENU_TYPE_LOCATION_SELECT);
        mb03.setName("Location菜单");
        mb03.setKey("menu_self_3");

        menu.setButton(new MenuButton[]{mb01, mb02, mb03});

        //打印验证字符串
        System.out.println(JSON.toJSONString(menu, true));

        //调用API生成菜单
        ApiResult apiResult = MenuApi.ice.createMenu(WxTestKit.getAccessToken(), menu.toJSON());
        System.out.println(apiResult.toJSON());
    }

    /**
     * 02 同时包含一级菜单和二级菜单
     */
    @Test
    public void menuMore() {
        Menu menu = new Menu();

        MenuButton mb01 = new MenuButton();
        mb01.setType(MenuApi.MENU_TYPE_SCANCODE_PUSH);
        mb01.setName("扫码推事件菜单");
        mb01.setKey("menu_scan_push");

        MenuButton mb02 = new MenuButton();
        mb02.setType(MenuApi.MENU_TYPE_SCANCODE_WAITMSG);
        mb02.setName("扫码推事件且显示Loading菜单");
        mb02.setKey("menu_scan_code_watimsg");

        MenuButton sub03 = new MenuButton();
        sub03.setName("我有子菜单");

        MenuButton mb31 = new MenuButton();
        mb31.setType(MenuApi.MENU_TYPE_CLICK);
        mb31.setName("子菜单点击");
        mb31.setKey("menu_sub_click31");

        MenuButton mb32 = new MenuButton();
        mb32.setType(MenuApi.MENU_TYPE_LOCATION_SELECT);
        mb32.setName("Location菜单");
        mb32.setKey("menu_sub_location32");

        sub03.setSub_button(new MenuButton[]{mb31, mb32});

        menu.setButton(new MenuButton[]{mb01, mb02, sub03});

        //打印验证字符串
        System.out.println(JSON.toJSONString(menu, true));

        //调用API生成菜单
        ApiResult apiResult = MenuApi.ice.createMenu(WxTestKit.getAccessToken(), menu.toJSON());
        System.out.println(apiResult.toJSON());
    }

    /**
     * 03 包含个性化菜单对象
     * 返回:400720360
     */
    @Test
    public void menuAddConditional() {
        Menu menu = new Menu();

        MenuButton mb01 = new MenuButton();
        mb01.setType(MenuApi.MENU_TYPE_CLICK);
        mb01.setName("个性化菜单男");
        mb01.setKey("menu_click_male");

        MenuMatchrule rule = new MenuMatchrule();
        rule.setSex(1);

        menu.setButton(new MenuButton[]{mb01});
        menu.setMatchrule(rule);

        //打印验证字符串
        System.out.println(JSON.toJSONString(menu, true));

        //调用API生成菜单
        ApiResult apiResult = MenuApi.ice.createConditionalMenu(WxTestKit.getAccessToken(), menu.toJSON());
        System.out.println(apiResult.toJSON());

    }

    /**
     * 查询当前的自定义菜单
     * 采用菜单对象转换自动转换自定义菜单数据为Menu对象
     * 测试包含个性化菜单的转换通过
     */
    @Test
    public void findMenuInfo() {
        ApiResult apiResult = MenuApi.ice.getMenu(WxTestKit.getAccessToken());
        if (apiResult.isSuccessful()) {
            Menu menu = MenuApi.ice.convert2Menu(apiResult.getJson());
            System.out.println(JSON.toJSONString(menu, true));
        }
    }

    /**
     * 查询当前自定义菜单的详细配置信息,包括对应的回复内容信息
     */
    @Test
    public void findCurrentMenuInfo() {
        ApiResult apiResult = MenuApi.ice.getCurrentMenuInfo(WxTestKit.getAccessToken());
        if (apiResult.isSuccessful()) {
            System.out.println(apiResult.getJson());
        }
    }

    /**
     * 删除自定义菜单, 同时也会删除个性化菜单
     */
    @Test
    public void deleteMenu() {
        ApiResult apiResult = MenuApi.ice.deleteMenu(WxTestKit.getAccessToken());
        System.out.println(apiResult.toJSON());
    }

    /**
     * 根据个性化菜单menuid删除指定的个性化菜单
     */
    @Test
    public void deleteConditionalMenu() {
        ApiResult apiResult = MenuApi.ice.deleteConditionalMenu(WxTestKit.getAccessToken(), "400721185");       //这里传查询出来的个性化菜单menuid
        if (apiResult.isSuccessful()) {
            System.out.println(apiResult.getJson());
        }
    }

    /**
     * 根据用户openid或者微信号查询该用户的当前自定义菜单,用户测试个性化菜单是否生效
     * <p/>
     */
    @Test
    public void tryMatchConditionalMenu() {
        ApiResult apiResult = MenuApi.ice.tryMatchConditionalMenu(WxTestKit.getAccessToken(), "ovpVQwi3EjIwLtOto9su82YsPenI");  //传openid或者微信号
        if (apiResult.isSuccessful()) {
            System.out.println(apiResult.getJson());
        }
    }
}

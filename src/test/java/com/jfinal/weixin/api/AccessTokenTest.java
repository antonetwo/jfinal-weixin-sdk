package com.jfinal.weixin.api;

import com.jfinal.weixin.api.model.AccessToken;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * access_token 测试
 */
public class AccessTokenTest {

    private String appid = "";
    private String appSecret = "";
    private String accessToken = "";

    /**
     * 测试独立的AccessTokenAPI
     */
    @Before()
    public void getAccessToken() {
        AccessToken at = AccessTokenApi.ice.getAccessToken(appid, appSecret);
        System.out.println(at);
        if (at != null) {
            this.accessToken = at.getAccess_token();
        }
    }

    /**
     * 测试获取微信服务器IP地址列表
     */
    @Test
    public void getWeixinCallbackIp() {
        List<String> ipList = AccessTokenApi.ice.getWeixinCallbackIp(accessToken);
        System.out.println("IP地址共:" + ipList.size());
        for (String ip : ipList) {
            System.out.println(ip);
        }
    }
}

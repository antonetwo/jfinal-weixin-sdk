package com.jfinal.weixin.api;

import com.alibaba.fastjson.JSON;
import com.jfinal.weixin.api.model.FollowList;
import com.jfinal.weixin.api.model.Follower;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-24
 */

/**
 * 用户管理接口测试
 */
public class UserApiTest {
    private static String accessToken = "";

    /**
     * 更新用户备注名
     */
    @Test
    public void updateRemark() {
        String openId = "ovpVQwi3EjIwLtOto9su82YsPenI";
        ApiResult result = UserApi.ice.updateRemark(accessToken, openId, "备注-老古");
        if (result.isSuccessful()) {
            System.out.println(result.getJson());
        }
    }

    /**
     * 获取用户资料信息
     */
    @Test
    public void getFollowerInfo() {
        String openId = "ovpVQwi3EjIwLtOto9su82YsPenI";
        ApiResult result = UserApi.ice.getFollowerInfo(accessToken, openId);
        if (result.isSuccessful()) {
            System.out.println(result.getJson());

            //转为对象
            Follower follower = UserApi.ice.convertFollower(result.getJson());
            if (follower != null) {
                System.out.println("得到对象:\n" + JSON.toJSONString(follower, true));
            }
        }
    }

    /**
     * 批量获取用户资料信息
     */
    @Test
    public void getFollowerList() {
        List<String> list = new ArrayList<>();
        list.add("ovpVQwi3EjIwLtOto9su82YsPenI");
        list.add("ovpVQwncFN7kseFCcw-YVt9ie8C4");
        list.add("ovpVQwtAHEKLzyGUeWFdSPHKNTvs");
        ApiResult result = UserApi.ice.getFollowerList(accessToken, list);
        if (result.isSuccessful()) {
            System.out.println(result.getJson());

            //转换为对象
            List<Follower> fList = UserApi.ice.convertFollowerList(result.getJson());
            if (fList != null && fList.size() > 0) {
                System.out.println("得到集合:\n" + JSON.toJSONString(fList, true));
            }
        }
    }

    /**
     * 获取用户列表数据, 返回OpenId集合
     */
    @Test
    public void getFollowerList_OpenId() {
        ApiResult result = UserApi.ice.getFollowerList(accessToken, "");
        if (result.isSuccessful()) {
            System.out.println(result.getJson());
            //转换为对象
            FollowList followList = UserApi.ice.convertFollowListForOpenId(result.getJson());
            if (followList != null) {
                System.out.println("得到的对象为:\n" + JSON.toJSONString(followList, true));
            }
        }
    }


}

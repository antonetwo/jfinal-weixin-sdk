package com.jfinal.weixin.api;

import com.jfinal.weixin.msg.out.Article;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-14
 */
public class ArticleListTest {
    @Test
    public void test() {
        List<Article> articleList = new ArrayList<>();
        for (int i = 1; i <= 11; i++) {
            Article article0 = new Article("title-" + i, "desc", "http://imgcache.qq.com/music/photo/album_300/60/300_albumpic_1215260_0.jpg", "http://www.baidu.com/");
            articleList.add(article0);
        }

        System.out.println(articleList.size());

        articleList = articleList.subList(0, 5);

        System.out.println(articleList.size());

    }
}

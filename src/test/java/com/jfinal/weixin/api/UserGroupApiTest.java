/*
 * Powered by LARRY.KOO(larrykoo@126.com) at 2015-12-19 19:39
 * Copyright (c) 2015, WeTeam Inc. All right reserved.
 * WeTeam PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * Project WeTeamCode
 * @version v1.0
 */
package com.jfinal.weixin.api;

import com.jfinal.weixin.api.model.UserGroup;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试微信用户用户组管理
 */
public class UserGroupApiTest {

    private static String accessToken = "";

    /**
     * 测试创建用户组
     */
    @Test
    public void create() {
        String groupName = "我是自定义分组";
        ApiResult result = UserGroupApi.ice.create(accessToken, groupName);
        System.out.println(result.toJSON());
    }

    /**
     * 获取所有用户组
     */
    @Test
    public void getAll() {
        List<UserGroup> list = UserGroupApi.ice.getAll(accessToken);
        for (UserGroup ug : list) {
            System.out.println(ug.toString());
        }
    }

    /**
     * 查询单个用户的分组ID
     */
    @Test
    public void getId() {
        String openId = "ovpVQwi3EjIwLtOto9su82YsPenI";
        ApiResult result = UserGroupApi.ice.getId(accessToken, openId);
        System.out.println(result.toJSON());
    }

    /**
     * 更新一个分组的名称
     */
    @Test
    public void updateName() {
        String groupId = "100";
        String groupNewName = "我是自定义分组-02";
        ApiResult result = UserGroupApi.ice.update(accessToken, groupId, groupNewName);
        System.out.println(result.toJSON());
    }

    /**
     * 移动用户到分组
     */
    @Test
    public void move() {
        String openId = "ovpVQwi3EjIwLtOto9su82YsPenI";
        Integer groupId = 100;
        ApiResult result = UserGroupApi.ice.membersUpdate(accessToken, openId, groupId);
        System.out.println(result.toJSON());
    }

    /**
     * 批量移动用户到分组
     */
    @Test
    public void moveBatch() {
        List<String> openIdList = new ArrayList<>();
        openIdList.add("ovpVQwi3EjIwLtOto9su82YsPenI");

        Integer groupId = 2;

        ApiResult result = UserGroupApi.ice.membersBatchUpdate(accessToken, openIdList, groupId);
        System.out.println(result.toJSON());
    }

    /**
     * 删除一个分组
     */
    @Test
    public void delete() {
        Integer groupId = 101;
        ApiResult result = UserGroupApi.ice.delete(accessToken, groupId);
        System.out.println(result.toJSON());
    }

}

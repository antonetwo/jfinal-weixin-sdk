package com.jfinal.utils.http;

import com.jfinal.utils.MediaFile;

import java.io.File;
import java.io.InputStream;
import java.util.Map;

/**
 * HTTP 工具请求类, 使用枚举类型实现单例模式
 * 调用方式为 HttpKit2.ice.get()
 */
public final class HttpUtil {

    // http请求工具代理对象
    private final static HttpDelegate delegate = new OkHttpDelegate();

    public static String get(String url) {
        return delegate.get(url);
    }

    public static String get(String url, Map<String, String> queryParas) {
        return delegate.get(url, queryParas);
    }

    public static String post(String url) {
        return delegate.post(url);
    }

    public static String post(String url, String data) {
        return delegate.post(url, data);
    }

    public static String post(String url, Map<String, String> queryParas) {
        return delegate.post(url, queryParas);
    }

    public static String postSSL(String url, String data, String certPath, String certPass) {
        return delegate.postSSL(url, data, certPath, certPass);
    }

    public static MediaFile download(String url) {
        return delegate.download(url);
    }

    public static InputStream download(String url, String params) {
        return delegate.download(url, params);
    }

    public static String upload(String url, File file, String params) {
        return delegate.upload(url, file, params);
    }
}

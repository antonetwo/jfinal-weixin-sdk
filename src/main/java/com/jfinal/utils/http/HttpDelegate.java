package com.jfinal.utils.http;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-18
 */

import com.jfinal.utils.MediaFile;

import java.io.File;
import java.io.InputStream;
import java.util.Map;

/**
 * http请求工具 委托接口
 */
public interface HttpDelegate {
    String get(String url);

    String get(String url, Map<String, String> queryParas);

    String post(String url);

    String post(String url, String data);

    String post(String url, Map<String, String> queryParas);

    String postSSL(String url, String data, String certPath, String certPass);

    MediaFile download(String url);

    InputStream download(String url, String params);

    String upload(String url, File file, String params);
}

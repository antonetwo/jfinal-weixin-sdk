package com.jfinal.utils;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-28
 */

import org.apache.commons.lang3.StringUtils;

/**
 * 对emoji表情字符的处理
 */
public class EmojiKit {
    /**
     * 检测是否有emoji字符
     *
     * @param source
     * @return 一旦含有就抛出
     */
    public Boolean containsEmoji(String source) {
        source += " ";
        if (StringUtils.isBlank(source)) {
            return false;
        }
        Boolean hasEmoji = false;

        int len = source.length();

        for (int i = 0; i < len; i++) {
            char codePoint = source.charAt(i);

            if (isEmojiCharacter(codePoint)) {
                // do nothing，判断到了这里表明，确认有表情字符
                hasEmoji = true;
                break;
            }
        }

        return hasEmoji;
    }

    private Boolean isEmojiCharacter(char codePoint) {
        return (codePoint == 0x0) || (codePoint == 0x9)
                || (codePoint == 0xA) || (codePoint == 0xD)
                || ((codePoint >= 0x20) && (codePoint <= 0xD7FF))
                || ((codePoint >= 0xE000) && (codePoint <= 0xFFFD))
                || ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));
    }

    /**
     * 过滤emoji 或者 其他非文字类型的字符
     *
     * @param source
     * @return
     */
    public String filterEmoji(String source) {
        source += " ";
        if (!containsEmoji(source)) {
            return StringUtils.trim(source);    // 如果不包含，直接返回
        }
        // 到这里铁定包含
        StringBuilder buf = null;

        int len = source.length();

        for (int i = 0; i < len; i++) {
            char codePoint = source.charAt(i);

            if (isEmojiCharacter(codePoint)) {
                if (buf == null) {
                    buf = new StringBuilder(source.length());
                }

                buf.append(codePoint);
            } else {
            }
        }

        if (buf == null) {
            return StringUtils.trim(source);    // 如果没有找到 emoji表情，则返回源字符串
        } else {
            if (buf.length() == len) {    // 这里的意义在于尽可能少的toString，因为会重新生成字符串
                return StringUtils.trim(source);
            } else {
                return StringUtils.trim(buf.toString());
            }
        }

    }
}

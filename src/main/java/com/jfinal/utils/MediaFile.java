package com.jfinal.utils;

import lombok.Data;

import java.io.BufferedInputStream;

/**
 * 素材文件，参考自____′↘夏悸 wechat
 *
 * @author L.cm
 */
@Data
public class MediaFile {
    private String fileName;
    private String fullName;
    private String suffix;
    private String contentLength;
    private String contentType;
    private BufferedInputStream fileStream;
    private String error;
}

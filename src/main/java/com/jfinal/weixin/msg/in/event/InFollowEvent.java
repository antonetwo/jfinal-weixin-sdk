package com.jfinal.weixin.msg.in.event;

import lombok.Data;

/**
 * 接收 关注/取消关注事件
 * <p/>
 * <xml>
 * <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[FromUser]]></FromUserName>
 * <CreateTime>123456789</CreateTime>
 * <MsgType><![CDATA[event]]></MsgType>
 * <Event><![CDATA[subscribe]]></Event>
 * <EventKey><![CDATA[eventKey]]></EventKey>
 * </xml>
 */
@Data
public class InFollowEvent extends InEventMsg {

    // 订阅：subscribe
    public static final String EVENT_INFOLLOW_SUBSCRIBE = "subscribe";
    // 取消订阅：unsubscribe
    public static final String EVENT_INFOLLOW_UNSUBSCRIBE = "unsubscribe";

    protected String eventKey;

    public InFollowEvent(String toUserName, String fromUserName, Integer createTime, String msgType, String event) {
        super(toUserName, fromUserName, createTime, msgType, event);
    }

}







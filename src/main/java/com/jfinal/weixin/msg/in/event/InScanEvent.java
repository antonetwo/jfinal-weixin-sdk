package com.jfinal.weixin.msg.in.event;

import lombok.Data;

/**
 * 扫码事件
 *
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-11
 */

/**
 * 扫描带参数二维码事件
 * 1. 用户未关注时，进行关注后的事件推送
 * <xml>
 * <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[FromUser]]></FromUserName>
 * <CreateTime>123456789</CreateTime>
 * <MsgType><![CDATA[event]]></MsgType>
 * <Event><![CDATA[subscribe]]></Event>
 * <EventKey><![CDATA[qrscene_123123]]></EventKey>
 * <Ticket><![CDATA[TICKET]]></Ticket>
 * </xml>
 * <p/>
 * 2. 用户已关注时的事件推送
 * <xml>
 * <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[FromUser]]></FromUserName>
 * <CreateTime>123456789</CreateTime>
 * <MsgType><![CDATA[event]]></MsgType>
 * <Event><![CDATA[SCAN]]></Event>
 * <EventKey><![CDATA[SCENE_VALUE]]></EventKey>
 * <Ticket><![CDATA[TICKET]]></Ticket>
 * </xml>
 */
@Data
public class InScanEvent extends InEventMsg {

    // 1. 用户未关注时，进行关注后的事件推送： subscribe
    public static final String EVENT_INQRCODE_SUBSCRIBE = "subscribe";
    // 2. 用户已关注时的事件推送： SCAN
    public static final String EVENT_INQRCODE_SCAN = "SCAN";

    // 1. 用户未关注时，进行关注后的事件推送： qrscene_123123
    // 2. 用户已关注时的事件推送： SCENE_VALUE
    private String eventKey;

    /**
     * 二维码的ticket,可用来换取二维码图片
     */
    private String ticket;

    public InScanEvent(String toUserName, String fromUserName, Integer createTime, String msgType, String event) {
        super(toUserName, fromUserName, createTime, msgType, event);
    }
}

package com.jfinal.weixin.msg.in.event.menu;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 相片MD5信息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PicItem {

    /**
     * 图片的MD5值,开发者若需要,可用于验证接收到图片
     */
    private String picMd5Sum;
}

package com.jfinal.weixin.msg.in.event.menu;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 菜单二维码扫描的结果实体
 *
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScanCodeInfo {

    /**
     * 扫描类型,一般是qrcode
     */
    private String scanType;
    /**
     * 扫描结果,即二维码对应的字符串信息
     */
    private String scanResult;

}

package com.jfinal.weixin.msg.in;

import lombok.Data;

/**
 * 接收消息基础类
 *
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-10
 */

/**
 * 接收消息，以下是接收文本消息的例子
 * 接收文本消息
 * <xml>
 * <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>1348831860</CreateTime>
 * <MsgType><![CDATA[text]]></MsgType>
 * <Content><![CDATA[this is a test]]></Content>
 * <MsgId>1234567890123456</MsgId>
 * </xml>
 */
@Data
public abstract class InMsg {
    /**
     * 微信公众号Id/OpenId
     */
    protected String toUserName;
    /**
     * OpenId/微信公众号Id
     */
    protected String fromUserName;
    /**
     * 消息创建时间 (整型)
     */
    protected Integer createTime;
    /**
     * 被动响应消息类型
     * 1：text 文本消息
     * 2：image 图片消息
     * 3：voice 语音消息
     * 4：video 视频消息
     * 5：location 地址位置消息
     * 6：link 链接消息
     * 7：event 事件
     */
    protected String msgType;

    public InMsg(String toUserName, String fromUserName, Integer createTime, String msgType) {
        this.toUserName = toUserName;
        this.fromUserName = fromUserName;
        this.createTime = createTime;
        this.msgType = msgType;
    }
}

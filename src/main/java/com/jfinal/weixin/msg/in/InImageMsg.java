package com.jfinal.weixin.msg.in;

import lombok.Data;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-10
 */

/**
 * 接收的图像消息
 * <p/>
 * <xml>
 * <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>1348831860</CreateTime>
 * <MsgType><![CDATA[image]]></MsgType>
 * <PicUrl><![CDATA[this is a url]]></PicUrl>
 * <MediaId><![CDATA[media_id]]></MediaId>
 * <MsgId>1234567890123456</MsgId>
 * </xml>
 */
@Data
public class InImageMsg extends InMsg {

    /**
     * 图片链接
     */
    private String picUrl;
    /**
     * 图片消息媒体id，可以调用多媒体文件下载接口拉取数据
     */
    private String mediaId;
    /**
     * 消息ID, 64位整型
     */
    private String msgId;

    public InImageMsg(String toUserName, String fromUserName, Integer createTime, String msgType) {
        super(toUserName, fromUserName, createTime, msgType);
    }
}

package com.jfinal.weixin.msg.in.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 摇一摇对象
 *
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AroundBeacon {
    private String uuid;
    private Integer major;
    private Integer minor;
    private Float distance;//设备与用户的距离（浮点数；单位：米）
}

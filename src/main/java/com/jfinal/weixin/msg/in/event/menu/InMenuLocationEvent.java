package com.jfinal.weixin.msg.in.event.menu;

import com.jfinal.weixin.msg.in.event.InEventMsg;
import lombok.Data;

/**
 * 自定义菜单推地理位置信息
 */
@Data
public class InMenuLocationEvent extends InEventMsg {

    private String eventKey;
    private ScanCodeInfo scanCodeInfo;
    /**
     * X坐标信息
     */
    private String locationX;
    /**
     * Y坐标信息
     */
    private String locationY;
    /**
     * 精度,可理解为精度或者比例尺,越精细的话 scale越高
     */
    private int scale;
    /**
     * 地理位置的字符串信息
     */
    private String label;
    /**
     * 朋友圈POI的名字,可能为空
     */
    private String poiname;


    public InMenuLocationEvent(String toUserName, String fromUserName, Integer createTime, String msgType, String event) {
        super(toUserName, fromUserName, createTime, msgType, event);
    }
}

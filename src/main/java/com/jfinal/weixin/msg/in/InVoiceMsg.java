package com.jfinal.weixin.msg.in;

import lombok.Data;

/**
 * 接收的音频消息
 *
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-10
 */

/**
 * <xml>
 * <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>1357290913</CreateTime>
 * <MsgType><![CDATA[voice]]></MsgType>
 * <MediaId><![CDATA[media_id]]></MediaId>
 * <Format><![CDATA[Format]]></Format>
 * <MsgId>1234567890123456</MsgId>
 * </xml>
 */
@Data
public class InVoiceMsg extends InMsg {
    /**
     * 语音消息媒体id,可以调用多媒体文件下载接口拉取数据
     */
    private String mediaId;
    /**
     * 语音格式,如amr,speex等
     */
    private String format;
    /**
     * 语音识别结果,UTF8编码
     */
    private String recognition;
    /**
     * 消息ID, 64位整型
     */
    private String msgId;

    public InVoiceMsg(String toUserName, String fromUserName, Integer createTime, String msgType) {
        super(toUserName, fromUserName, createTime, msgType);
    }
}

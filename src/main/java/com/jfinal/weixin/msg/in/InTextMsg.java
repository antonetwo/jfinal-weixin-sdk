package com.jfinal.weixin.msg.in;

import lombok.Data;

/**
 * 接收的文本消息
 *
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-10
 */

/**
 * <xml>
 * <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>1348831860</CreateTime>
 * <MsgType><![CDATA[text]]></MsgType>
 * <Content><![CDATA[this is a test]]></Content>
 * <MsgId>1234567890123456</MsgId>
 * </xml>
 */
@Data
public class InTextMsg extends InMsg {
    /**
     * 文本内容
     */
    private String content;
    /**
     * 消息ID, 64位整型
     */
    private String msgId;

    public InTextMsg(String toUserName, String fromUserName, Integer createTime, String msgType) {
        super(toUserName, fromUserName, createTime, msgType);
    }
}

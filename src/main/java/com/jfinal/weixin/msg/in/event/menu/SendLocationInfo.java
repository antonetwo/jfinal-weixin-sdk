package com.jfinal.weixin.msg.in.event.menu;

import lombok.Data;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-13
 */
@Data
public class SendLocationInfo {
    /**
     * X坐标信息
     */
    private String locationX;
    /**
     * Y坐标信息
     */
    private String locationY;
    /**
     * 精度,可理解为精度或者比例尺,越精细的话 scale越高
     */
    private int scale;
    /**
     * 地理位置的字符串信息
     */
    private String label;
    /**
     * 朋友圈POI的名字,可能为空
     */
    private String poiname;
}

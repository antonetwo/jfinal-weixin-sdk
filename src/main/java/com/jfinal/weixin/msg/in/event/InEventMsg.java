package com.jfinal.weixin.msg.in.event;

import com.jfinal.weixin.msg.in.InMsg;
import lombok.Data;

/**
 * 接收事件消息
 */
@Data
public abstract class InEventMsg extends InMsg {

    /**
     * 事件类型:subscribe(订阅),unsubscribe(取消订阅)...
     */
    protected String event;

    public InEventMsg(String toUserName, String fromUserName, Integer createTime, String msgType, String event) {
        super(toUserName, fromUserName, createTime, msgType);
        this.event = event;
    }

}

package com.jfinal.weixin.msg.in.event.menu;

import lombok.Data;

import java.util.List;

/**
 * 扫码发图像信息
 */
@Data
public class SendPicsInfo {

    /**
     * 发送的图片数量
     */
    private int count;
    /**
     * 图片列表
     */
    private List<PicItem> picList;
}

package com.jfinal.weixin.msg.in;

import lombok.Data;

/**
 * 接收的视频消息
 *
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-10
 */

/**
 * <xml>
 * <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>1357290913</CreateTime>
 * <MsgType><![CDATA[video]]></MsgType>
 * <MediaId><![CDATA[media_id]]></MediaId>
 * <ThumbMediaId><![CDATA[thumb_media_id]]></ThumbMediaId>
 * <MsgId>1234567890123456</MsgId>
 * </xml>
 */
@Data
public class InVideoMsg extends InMsg {
    /**
     * 视频消息媒体id,可以调用多媒体文件下载接口拉取数据
     */
    private String mediaId;
    /**
     * 视频消息缩略图的媒体id,可以调用多媒体文件下载接口拉取数据
     */
    private String thumbMediaId;
    /**
     * 视频消息的标题
     */
    private String title;
    /**
     * 视频消息的描述
     */
    private String description;
    /**
     * 消息ID, 64位整型
     */
    private String msgId;

    public InVideoMsg(String toUserName, String fromUserName, Integer createTime, String msgType) {
        super(toUserName, fromUserName, createTime, msgType);
    }

}

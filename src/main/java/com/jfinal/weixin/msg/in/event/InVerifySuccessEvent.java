package com.jfinal.weixin.msg.in.event;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-12
 */

import lombok.Data;

/**
 * 资质认证成功通知
 * <p/>
 * <xml><ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[FromUser]]></FromUserName>
 * <CreateTime>1442401156</CreateTime>
 * <MsgType><![CDATA[event]]></MsgType>
 * <Event><![CDATA[qualification_verify_success]]></Event>
 * <ExpiredTime>1442401156</ExpiredTime>
 * </xml>
 */
@Data
public class InVerifySuccessEvent extends InEventMsg {
    //资质认证成功
    public static final String EVENT_IN_QUALIFICATION_VERIFY_SUCCESS = "qualification_verify_success";
    //名称认证成功
    public static final String EVENT_IN_NAMING_VERIFY_SUCCESS = "naming_verify_success";
    //年审通知
    public static final String EVENT_IN_ANNUAL_RENEW = "annual_renew";
    //认证过期失效通知
    public static final String EVENT_IN_VERIFY_EXPIRED = "verify_expired";

    private String expiredTime;

    public InVerifySuccessEvent(String toUserName, String fromUserName, Integer createTime, String msgType, String event) {
        super(toUserName, fromUserName, createTime, msgType, event);
    }

}

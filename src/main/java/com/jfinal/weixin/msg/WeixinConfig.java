/**
 * Copyright (c) 2011-2014, James Zhan 詹波 (jfinal@126.com).
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.jfinal.weixin.msg;

import com.jfinal.weixin.mvc.MessageProcessingHandler;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

/**
 * 存放 Weixin 服务号需要用到的各个参数
 */
@Data
@AllArgsConstructor
public class WeixinConfig {
    /**
     * 消息验证token
     */
    @NonNull
    private String token;

    /**
     * 公众号appId
     */
    @NonNull
    private String appId;

    /**
     * 公众号appSecret
     */
    private String appSecret;

    /**
     * 安全模式下的AES加密密钥
     */
    private String encodingAesKey;

    /**
     * 微信消息处理驱动
     */
    private MessageProcessingHandler messageHandler;

    /**
     * 运行模式, true=开发模式
     */
    private boolean devMode = true;

    public WeixinConfig() {

    }

    public WeixinConfig(String token) {
        setToken(token);
    }

    public WeixinConfig(String token, String appId, String appSecret) {
        setToken(token);
        setAppId(appId);
        setAppSecret(appSecret);
    }
}



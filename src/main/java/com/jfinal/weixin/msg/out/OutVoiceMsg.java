package com.jfinal.weixin.msg.out;

import com.jfinal.weixin.msg.in.InMsg;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 回复的音频消息
 *
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-10
 */
@Data
public class OutVoiceMsg extends OutMsg {
    /**
     * 消息包含的媒体资源对象
     */
    @XStreamAlias("Voice")
    private Voice voice;

    public OutVoiceMsg() {
        super();
        this.msgType = "voice";
    }

    public OutVoiceMsg(InMsg inMsg) {
        super(inMsg);
        this.msgType = "voice";
    }

    public OutVoiceMsg setMediaId(String mediaId) {
        this.voice = new Voice(mediaId);
        return this;
    }

    /**
     * 内联语音资源对象
     */
    @Data
    @AllArgsConstructor
    static class Voice {
        /**
         * 消息媒体id，调用多媒体文件上传接口返回
         */
        @XStreamAlias("MediaId")
        private String mediaId;
    }
}

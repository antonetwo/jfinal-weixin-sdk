package com.jfinal.weixin.msg.out;

import com.jfinal.weixin.msg.in.InMsg;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * 回复的文本消息
 *
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-10
 */
@Data
public class OutTextMsg extends OutMsg {

    /**
     * 文本内容
     */
    @XStreamAlias("Content")
    private String content;

    public OutTextMsg() {
        this.msgType = "text";
    }

    public OutTextMsg(InMsg inMsg) {
        super(inMsg);
        this.msgType = "text";
    }

    public OutTextMsg setContent(String content) {
        this.content = content;
        return this;
    }

}

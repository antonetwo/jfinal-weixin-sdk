package com.jfinal.weixin.msg.out;

import com.jfinal.utils.Collections3;
import com.jfinal.weixin.msg.in.InMsg;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 多图文消息
 *
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-10
 */
@Data
public class OutNewsMsg extends OutMsg {

    /**
     * 图文消息个数,限制为10条以内,已废除
     */
    @XStreamAlias("ArticleCount")
    private Integer articleCount;
    /**
     * 多条图文消息信息,默认第一个item为大图
     * </p>
     * 注意:如果图文数超过10,则将会无响应
     */
    @XStreamAlias("Articles")
    private List<Article> articles = new ArrayList<>();

    public OutNewsMsg build() {
        setArticles(this.getArticles());
        return this;
    }

    public OutNewsMsg() {
        super();
        this.msgType = "news";
    }

    public OutNewsMsg(InMsg msg) {
        super(msg);
        this.msgType = "news";
    }

    public List<Article> getArticles() {
        if (!Collections3.isEmpty(articles) && articles.size() > 10) {
//            this.articles = articles.subList(0, 10);      //采用subList裁剪返回为子列表,无法倍XStream正常解析
            List<Article> newArticles = new ArrayList<>();
            for (int i = 0; i <= 10; i++) {
                newArticles.add(articles.get(i));
            }
            setArticles(newArticles);
            setArticleCount(10);
        } else {
            this.setArticleCount(articles.size());
        }
        return articles;
    }

    public void setArticles(List<Article> articles) {
        if (articles != null) {
            this.articles = articles;
        }
    }

    public OutNewsMsg addNews(List<Article> articles) {
        if (articles != null)
            this.articles.addAll(articles);
        return this;
    }

    public OutNewsMsg addNews(String title, String description, String picUrl, String url) {
        this.articles.add(new Article(title, description, picUrl, url));
        return this;
    }

    public OutNewsMsg addNews(Article article) {
        this.articles.add(article);
        return this;
    }

}

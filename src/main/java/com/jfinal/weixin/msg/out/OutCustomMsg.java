package com.jfinal.weixin.msg.out;

import lombok.Data;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-13
 */

/**
 * 转发多客服消息
 * <xml>
 * <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>12345678</CreateTime>
 * <MsgType><![CDATA[transfer_customer_service]]></MsgType>
 * </xml>
 */
@Data
public class OutCustomMsg extends OutMsg {

    private String content;

    public OutCustomMsg setContent(String content) {
        this.content = content;
        return this;
    }

    public OutCustomMsg() {
        this.msgType = "transfer_customer_service";
    }
}

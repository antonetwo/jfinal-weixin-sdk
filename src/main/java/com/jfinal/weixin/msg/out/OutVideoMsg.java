package com.jfinal.weixin.msg.out;

import com.jfinal.weixin.msg.in.InMsg;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-11
 */
@Data
public class OutVideoMsg extends OutMsg {
    /**
     * 消息包含的媒体资源对象
     */
    @XStreamAlias("Video")
    private Video video;

    public OutVideoMsg() {
        super();
        this.msgType = "video";
    }

    public OutVideoMsg(InMsg inMsg) {
        super(inMsg);
        this.msgType = "video";
    }

    public OutVideoMsg setVideo(String mediaId) {
        this.video = new Video(mediaId);
        return this;
    }

    public OutVideoMsg setVideo(String mediaId, String title, String description) {
        this.video = new Video(mediaId, title, description);
        return this;
    }

    /**
     * 内联Video资源对象
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class Video {
        /**
         * 消息媒体id，调用多媒体文件上传接口返回
         */
        @XStreamAlias("MediaId")
        private String mediaId;

        @XStreamAlias("Title")
        private String title;

        @XStreamAlias("Description")
        private String description;

        public Video(String mediaId) {
            this.mediaId = mediaId;
        }
    }
}

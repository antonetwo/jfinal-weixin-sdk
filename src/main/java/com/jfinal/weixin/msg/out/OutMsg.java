package com.jfinal.weixin.msg.out;

import com.jfinal.weixin.msg.in.InMsg;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAliasType;
import lombok.Data;

/**
 * 回复消息基础类
 *
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-10
 */
@Data
public abstract class OutMsg {
    /**
     * 微信公众号Id/OpenId
     */
    @XStreamAlias("ToUserName")
    protected String toUserName;
    /**
     * OpenId/微信公众号Id
     */
    @XStreamAlias("FromUserName")
    protected String fromUserName;
    /**
     * 消息创建时间 (整型)
     */
    @XStreamAlias("CreateTime")
    protected Integer createTime;
    /**
     * 被动响应消息类型
     * 1：text 文本消息
     * 2：image 图片消息
     * 3：voice 语音消息
     * 4：video 视频消息
     * 5：music 音乐消息
     * 6：news 图文消息
     */
    @XStreamAlias("MsgType")
    protected String msgType;

    /**
     * 消息Id, 64位整型
     * 官方无该属性, 暂时预留做重复标识用
     */
    @XStreamAlias("MsgId")
    protected Long msgId;

    /**
     * 默认构造方法
     *
     */
    public Integer now() {
        return Long.valueOf(System.currentTimeMillis() / 1000).intValue();
    }

    public OutMsg() {

    }

    /**
     * 用接收到的消息初始化要发出去的消息，关键在于两者 toUserName 与 fromUserName 相反
     */
    public OutMsg(InMsg inMsg) {
        this.toUserName = inMsg.getFromUserName();
        this.fromUserName = inMsg.getToUserName();
        this.createTime = now();
    }
}

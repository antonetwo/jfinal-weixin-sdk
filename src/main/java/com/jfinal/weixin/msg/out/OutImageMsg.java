package com.jfinal.weixin.msg.out;

import com.jfinal.weixin.msg.in.InMsg;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 回复的图像消息
 *
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-10
 */
@Data
public class OutImageMsg extends OutMsg {

    /**
     * 消息包含的媒体资源对象
     */
    @XStreamAlias("Image")
    private Image image;

    public OutImageMsg() {
        super();
        this.msgType = "image";
    }

    public OutImageMsg(InMsg inMsg) {
        super(inMsg);
        this.msgType = "image";
    }

    public OutImageMsg setMediaId(String mediaId) {
        this.image = new Image(mediaId);
        return this;
    }

    /**
     * 内联图片资源对象
     */
    @Data
    @AllArgsConstructor
    static class Image {
        /**
         * 消息媒体id，调用多媒体文件上传接口返回
         */
        @XStreamAlias("MediaId")
        private String mediaId;
    }
}

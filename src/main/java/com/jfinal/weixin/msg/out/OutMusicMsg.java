package com.jfinal.weixin.msg.out;

import com.jfinal.weixin.msg.in.InMsg;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-11
 */
@Data
public class OutMusicMsg extends OutMsg {
    /**
     * 消息包含的媒体资源对象
     */
    @XStreamAlias("Music")
    private Music music;

    public OutMusicMsg() {
        super();
        this.msgType = "music";
    }

    public OutMusicMsg(InMsg inMsg) {
        super(inMsg);
        this.msgType = "music";
    }

    public OutMusicMsg setMusic(String title, String hqMusicUrl) {
        this.music = new Music(title, "", "", hqMusicUrl);
        return this;
    }

    public OutMusicMsg setMusic(String title, String description, String musicUrl, String hqMusicUrl) {
        this.music = new Music(title, description, musicUrl, hqMusicUrl);
        return this;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class Music {
        // 音乐标题, 不是必须
        @XStreamAlias("Title")
        private String title;

        // 音乐描述, 不是必须
        @XStreamAlias("Description")
        private String description;

        // 音乐文件地址, musicUrl 和 hqMusicUrl 至少填一个
        @XStreamAlias("MusicUrl")
        private String musicUrl;

        // 音乐文件高清地址
        @XStreamAlias("HQMusicUrl")
        private String hqMusicUrl;

        // 官方文档有误，该属性无效
        // private String thumbMediaId;

        public Music(String hqMusicUrl) {
            this.hqMusicUrl = hqMusicUrl;
        }
    }
}

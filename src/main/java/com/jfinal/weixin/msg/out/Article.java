package com.jfinal.weixin.msg.out;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 图文详情内容
 *
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Article {
    /**
     * 图文消息标题
     */
    @XStreamAlias("Title")
    private String title;
    /**
     * 图文消息描述
     */
    @XStreamAlias("Description")
    private String description;
    /**
     * 图片链接,支持JPG,PNG格式,
     * </p>
     * 较好的效果为大图360*200,小图200*200
     */
    @XStreamAlias("PicUrl")
    private String picUrl;
    /**
     * 点击图文消息跳转链接
     */
    @XStreamAlias("Url")
    private String url;
}

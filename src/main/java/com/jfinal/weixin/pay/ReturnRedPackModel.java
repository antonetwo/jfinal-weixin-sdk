package com.jfinal.weixin.pay;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2016-01-15
 */

/**
 * 微信支付红包请求返回结果对象
 */
@Slf4j
@Data
public class ReturnRedPackModel {
    private String return_code;         //返回状态码, SUCCESS/FAIL, 通信标识,非交易标识
    private String return_msg;          //返回信息

    //以下字段在return_code为SUCCESS的时候有返回
    private String sign;                //签名
    private String result_code;         //业务结果, SUCCESS/FAIL
    private String err_code;            //错误状态码, SYSTEMERROR
    private String err_code_des;        //错误信息描述

    //以下字段在return_code和result_code都为SUCCESS的时候有返回
    private String mch_billno;          //商户订单号
    private String mch_id;              //商户号
    private String wxappid;             //公众帐号appid
    private String re_openid;           //用户openId
    private String total_amount;        //付款金额,单位分
    private String send_time;           //发放成功时间
    private String send_listid;         //红包订单的微信单号
}

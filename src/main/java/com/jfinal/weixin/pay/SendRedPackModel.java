package com.jfinal.weixin.pay;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2016-01-15
 */

/**
 * 微信支付发送红包数据对象
 */
@Slf4j
@Data
public class SendRedPackModel {
    private String nonce_str;           //随机字符串, 最多32位
    private String sign;                //签名
    private String mch_billno;          //商户订单号, mch_id+yyyymmdd+10位随机
    private String mch_id;              //商户号
    private String wxappid;             //公众帐号appid
    private String send_name;           //商户名称
    private String re_openid;           //用户openid
    private String total_amount;        //付款金额
    private String total_num;           //红包发放总人数
    private String wishing;             //红包祝福语
    private String client_ip;           //调用接口的服务器IP地址
    private String act_name;            //活动名称
    private String remark;              //备注信息
}

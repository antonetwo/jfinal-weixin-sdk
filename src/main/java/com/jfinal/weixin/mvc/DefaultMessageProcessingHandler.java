package com.jfinal.weixin.mvc;

import com.jfinal.weixin.msg.in.*;
import com.jfinal.weixin.msg.in.event.*;
import com.jfinal.weixin.msg.out.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-13
 */
public class DefaultMessageProcessingHandler implements MessageProcessingHandler {

    private OutMsg outMessage;

    @Override
    public void processInTextMsg(InTextMsg inTextMsg) {

    }

    @Override
    public void processInImageMsg(InImageMsg inImageMsg) {

    }

    @Override
    public void processInVoiceMsg(InVoiceMsg inVoiceMsg) {

    }

    @Override
    public void processInVideoMsg(InVideoMsg inVideoMsg) {

    }

    @Override
    public void processInLocationMsg(InLocationMsg inLocationMsg) {

    }

    @Override
    public void processInLinkMsg(InLinkMsg inLinkMsg) {

    }

    @Override
    public void processInMenuEvent(InMenuEvent inMenuEvent) {

    }

    @Override
    public void processInCustomEvent(InCustomEvent inCustomEvent) {

    }

    @Override
    public void processInFollowEvent(InFollowEvent inFollowEvent) {

    }

    @Override
    public void processInQrCodeEvent(InScanEvent inScanEvent) {

    }

    @Override
    public void processInLocationEvent(InLocationEvent inLocationEvent) {

    }

    @Override
    public void processInMassEvent(InMassEvent inMassEvent) {

    }

    @Override
    public void processInTemplateMsgEvent(InTemplateMsgEvent inTemplateMsgEvent) {

    }

    @Override
    public void processInShakeAroundUserShakeEvent(InShakeAroundUserShakeEvent inShakeAroundUserShakeEvent) {

    }

    @Override
    public void processInVerifySuccessEvent(InVerifySuccessEvent inVerifySuccessEvent) {

    }

    @Override
    public void processInVerifyFailEvent(InVerifyFailEvent inVerifyFailEvent) {

    }

    /**
     * ======================================================
     * ==================== 驱动 ====================
     * ======================================================
     */

    @Override
    public void beforeProcess(InMsg msg) {
        //TODO 进入消息处理
        System.out.println("================ 消息处理开始驱动 ==================");

        //返回文本
        OutTextMsg out = new OutTextMsg(msg).setContent("您的消息已经收到!");
        setOutMessage(out);

        //返回图片 TODO 暂未测试
//        OutImageMsg outImage = new OutImageMsg(msg).setMediaId("123456");
//        setOutMessage(outImage);

        //返回语音 TODO 暂未测试
//        OutVoiceMsg outVoice = new OutVoiceMsg(msg).setMediaId("123456");
//        setOutMessage(outVoice);

        //返回视频  TODO 暂未测试
//        OutVideoMsg videoMsg = new OutVideoMsg(msg).setVideo("123456", "title", "desc");
//        setOutMessage(videoMsg);

//        //返回音乐
//        OutMusicMsg musicMsg = new OutMusicMsg(msg).setMusic("卡农", "http://stream17.qqmusic.qq.com/35004185.mp3");
//        setOutMessage(musicMsg);
//
//        //返回单条图文
//        Article article = new Article("title", "desc", "http://imgcache.qq.com/music/photo/album_300/60/300_albumpic_1215260_0.jpg", "http://www.baidu.com/");
//        OutNewsMsg newsMsg = new OutNewsMsg(msg).addNews(article).build();
//        setOutMessage(newsMsg);

        //返回多条图文
//        List<Article> articleList = new ArrayList<>();
//        for (int i = 1; i <= 11; i++) {
//            Article article0 = new Article("title-" + i, "desc", "http://imgcache.qq.com/music/photo/album_300/60/300_albumpic_1215260_0.jpg", "http://www.baidu.com/");
//            articleList.add(article0);
//        }
//        OutNewsMsg newsMsg = new OutNewsMsg(msg).addNews(articleList).build();
//        setOutMessage(newsMsg);

    }

    @Override
    public void afterProcess(InMsg inMsg, OutMsg outMsg) {

    }

    @Override
    public OutMsg getOutMessage() {
        return outMessage;
    }

    public void setOutMessage(OutMsg outMessage) {
        this.outMessage = outMessage;
    }
}

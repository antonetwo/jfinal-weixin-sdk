package com.jfinal.weixin.mvc;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-13
 */

import com.jfinal.weixin.msg.in.*;
import com.jfinal.weixin.msg.in.event.*;
import com.jfinal.weixin.msg.out.OutMsg;

/**
 * 处理微信消息的接口
 */
public interface MessageProcessingHandler {

    // 处理接收到的文本消息
    void processInTextMsg(InTextMsg inTextMsg);

    // 处理接收到的图片消息
    void processInImageMsg(InImageMsg inImageMsg);

    // 处理接收到的语音消息
    void processInVoiceMsg(InVoiceMsg inVoiceMsg);

    // 处理接收到的视频消息
    void processInVideoMsg(InVideoMsg inVideoMsg);

    // 处理接收到的地址位置消息
    void processInLocationMsg(InLocationMsg inLocationMsg);

    // 处理接收到的链接消息
    void processInLinkMsg(InLinkMsg inLinkMsg);

    // 处理接收到的自定义菜单事件
    void processInMenuEvent(InMenuEvent inMenuEvent);

    // 处理接收到的多客服管理事件
    void processInCustomEvent(InCustomEvent inCustomEvent);

    // 处理接收到的关注/取消关注事件
    void processInFollowEvent(InFollowEvent inFollowEvent);

    // 处理接收到的扫描带参数二维码事件
    void processInQrCodeEvent(InScanEvent inScanEvent);

    // 处理接收到的上报地理位置事件
    void processInLocationEvent(InLocationEvent inLocationEvent);

    // 处理接收到的群发任务结束时通知事件
    void processInMassEvent(InMassEvent inMassEvent);

    // 处理接收到的模板消息是否送达成功通知事件
    void processInTemplateMsgEvent(InTemplateMsgEvent inTemplateMsgEvent);

    // 处理微信摇一摇事件
    void processInShakeAroundUserShakeEvent(InShakeAroundUserShakeEvent inShakeAroundUserShakeEvent);

    // 资质认证成功 || 名称认证成功 || 年审通知 || 认证过期失效通知
    void processInVerifySuccessEvent(InVerifySuccessEvent inVerifySuccessEvent);

    // 资质认证失败 || 名称认证失败
    void processInVerifyFailEvent(InVerifyFailEvent inVerifyFailEvent);

    /**
     * ======================================================
     * ==================== Handler 驱动处理器 ====================
     * ======================================================
     */

    //处理流程开始, 消息进入时执行
    void beforeProcess(InMsg msg);

    //处理流程结束，返回输出信息之前执行
    void afterProcess(InMsg inMsg, OutMsg outMsg);

    void setOutMessage(OutMsg outMessage);

    OutMsg getOutMessage();
}

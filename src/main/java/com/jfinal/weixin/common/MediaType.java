package com.jfinal.weixin.common;

/**
 * 多媒体文件类型
 *
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-10
 * <p/>
 * 代码来自: mpsdk4j的凡梦星尘(elkan1788@gmail.com)
 */
public enum MediaType {

    /**
     * 图片: 1M,支持JPG格式
     */
    image,
    /**
     * 语音:2M,播放长度不超过60s,支持AMR\MP3格式
     */
    voice,
    /**
     * 视频:10MB,支持MP4格式
     */
    video,
    /**
     * 缩略图:64KB,支持JPG格式
     */
    thumb
}

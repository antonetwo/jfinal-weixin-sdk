/**
 * Copyright (c) 2011-2014, James Zhan 詹波 (jfinal@126.com).
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.jfinal.weixin.kit;

import com.jfinal.core.Controller;
import com.jfinal.kit.HashKit;
import com.jfinal.weixin.msg.WeixinConfig;

import java.util.Arrays;

/**
 * 测试用的账号：
 * appID = wx9803d1188fa5fbda
 * appsecret = db859c968763c582794e7c3d003c3d87
 * url = http://www.jfinal.com/weixin
 * token = __my__token__
 */
public enum SignatureCheckKit {

    ice;

    /**
     * 验证微信请求签名合法性
     *
     * @return
     */
    public boolean checkSignature(String mpToken, String signature, String timestamp, String nonce) {
        String array[] = {mpToken, timestamp, nonce};
        Arrays.sort(array);
        String tempStr = new StringBuilder().append(array[0] + array[1] + array[2]).toString();
        tempStr = HashKit.sha1(tempStr);
        return tempStr.equalsIgnoreCase(signature);
    }

    public boolean checkSignature(Controller c, WeixinConfig wxConfig) {
        return checkSignature(wxConfig.getToken(), c.getPara("signature"), c.getPara("timestamp"), c.getPara("nonce"));
    }
}




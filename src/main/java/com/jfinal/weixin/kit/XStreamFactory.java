package com.jfinal.weixin.kit;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.XppDriver;
import org.apache.commons.lang3.StringUtils;

import java.io.Writer;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-14
 * <p/>
 * 使用enum方式保持单例
 */
public enum XStreamFactory {
    ice;

    protected static String PREFIX_CDATA = "<![CDATA[";
    protected static String SUFFIX_CDATA = "]]>";

    /**
     * 初始化XStream 可支持某一字段可以加入CDATA标签 如果需要某一字段使用原文
     * 就需要在String类型的text的头加上"<![CDATA["和结尾处加上"]]>"标签， 以供XStream输出时进行识别
     *
     * @param isAddCDATA 是否支持CDATA标签
     * @return
     */
    public XStream init(boolean isAddCDATA) {
        XStream xstream;
        if (isAddCDATA) {
            xstream = new XStream(new XppDriver() {
                public HierarchicalStreamWriter createWriter(Writer out) {
                    return new PrettyPrintWriter(out) {
                        @SuppressWarnings("rawtypes")
                        public void startNode(String name, Class clazz) {
                            super.startNode(name, clazz);
                        }

                        protected void writeText(QuickWriter writer, String text) {
                            if (!text.startsWith(PREFIX_CDATA) && !StringUtils.isNumeric(text)) {
                                text = PREFIX_CDATA + text + SUFFIX_CDATA;
                            }
                            writer.write(text);
                        }
                    };
                }
            });
        } else {
            xstream = new XStream();
        }
        return xstream;
    }
}

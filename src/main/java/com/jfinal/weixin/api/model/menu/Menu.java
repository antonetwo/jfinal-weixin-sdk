package com.jfinal.weixin.api.model.menu;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-16
 */

import com.alibaba.fastjson.JSON;
import lombok.Data;

import java.util.List;

/**
 * 自定义菜单对象
 */
@Data
public class Menu {
    /**
     * @Fields button : 一级菜单数组，个数应为1~3个
     */
    private MenuButton[] button;

    /**
     * 创建个性化自定义菜单规则
     */
    private MenuMatchrule matchrule;

    /**
     * 查询菜单数据返回的个性化菜单数据
     */
    private List<ConditionalMenu> conditionalMenu;

    /**
     * 判断是否包含个性化菜单规则
     *
     * @return
     */
    public Boolean hasConditionalMenu() {
        if (this.getMatchrule() != null || conditionalMenu.size() > 0) {
            return true;
        }
        return false;
    }

    /**
     * 转换Menu对象为JSON字符串
     *
     * @return
     */
    public String toJSON() {
        return JSON.toJSONString(this);
    }

}

package com.jfinal.weixin.api.model.menu;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-16
 */

/**
 * 基础自定义菜单按钮包含的属性
 */
@Data
public class MenuButton implements Serializable {

    private static final long serialVersionUID = -5957241240941202189L;

    /**
     * 菜单响应动作类型
     */
    private String type;

    /**
     * 菜单标题，不超过16个字节，子菜单不超过40个字节
     */
    private String name;

    /**
     * click等点击类型必须
     * 菜单KEY值，用于消息接口推送，不超过128字节
     */
    private String key;

    /**
     * view类型必须
     * 网页链接，用户点击菜单可打开链接，不超过256字节
     */
    private String url;

    /**
     * media_id类型和view_limited类型必须
     * 调用新增永久素材接口返回的合法media_id
     */
    private String media_id;

    /**
     * 二级菜单数组，个数应为1~5个
     */
    private MenuButton[] sub_button;
}

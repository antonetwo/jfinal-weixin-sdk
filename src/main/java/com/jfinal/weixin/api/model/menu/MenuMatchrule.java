package com.jfinal.weixin.api.model.menu;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-17
 */

/**
 * 个性化菜单匹配规则
 */
@Data
public class MenuMatchrule implements Serializable {

    /**
     * 用户分组id，可通过用户分组管理接口获取
     */
    private String group_id;

    /**
     * 性别：男（1）女（2），不填则不做匹配
     */
    private Integer sex;

    /**
     * 客户端版本，当前只具体到系统型号：IOS(1), Android(2),Others(3)，不填则不做匹配
     */
    private String client_platform_type;

    /**
     * 国家信息，是用户在微信中设置的地区，具体请参考地区信息表
     */
    private String country;

    /**
     * 省份信息，是用户在微信中设置的地区，具体请参考地区信息表
     */
    private String province;

    /**
     * 城市信息，是用户在微信中设置的地区，具体请参考地区信息表
     */
    private String city;
}

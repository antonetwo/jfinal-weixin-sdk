package com.jfinal.weixin.api.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-24
 */

/**
 * 批量获取用户openid集合返回对象
 */
@Data
public class FollowList {
    /**
     * 关注该公众账号的总用户数
     */
    private int total;
    /**
     * 拉取的OPENID个数,最大值为10000
     */
    private int count;
    /**
     * 列表数据,OPENID的列表
     */
    private List<String> openIds = new ArrayList<>();
    /**
     * 拉取列表的后一个用户的OPENID
     */
    private String next_openid;
}

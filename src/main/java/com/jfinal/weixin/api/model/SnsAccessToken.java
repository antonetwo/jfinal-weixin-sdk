package com.jfinal.weixin.api.model;

import com.jfinal.utils.RetryUtils;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-25
 */
@Data
public class SnsAccessToken implements RetryUtils.ResultCheck, Serializable {
    
    private static final long serialVersionUID = 464588841844550662L;

    //网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
    private String access_token;

    //access_token接口调用凭证超时时间，单位（秒）
    private Integer expires_in;

    //用户刷新access_token
    private String refresh_token;

    //用户唯一标识，请注意，在未关注公众号时，用户访问公众号的网页，也会产生一个用户和公众号唯一的OpenID
    private String openid;

    //用户授权的作用域，使用逗号（,）分隔
    private String scope;

    //当且仅当该公众号已获取用户的userinfo授权，并且该公众号已经绑定到微信开放平台帐号时，才会出现该字段
    private String unionid;

    /**
     * 时间验证
     *
     * @return
     */
    public boolean isAvailable() {
        if (StringUtils.isNotBlank(access_token) && this.expires_in > 1) {
            return true;
        }
        return false;
    }

    @Override
    public boolean matching() {
        return isAvailable();
    }
}

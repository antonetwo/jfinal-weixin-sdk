package com.jfinal.weixin.api.model;

import lombok.Data;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-21
 */
@Data
public class UserGroup {
    /**
     * 分组id,由微信分配
     */
    private int id;
    /**
     * 分组名字,UTF8编码
     */
    private String name;
    /**
     * 分组内用户数量
     */
    private int count;
}

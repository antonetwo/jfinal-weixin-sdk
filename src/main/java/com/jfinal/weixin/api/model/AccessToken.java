package com.jfinal.weixin.api.model;

import com.jfinal.utils.RetryUtils;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * 微信API凭证
 */
@Data
public class AccessToken implements RetryUtils.ResultCheck, Serializable {

    private static final long serialVersionUID = -1117789141828249051L;
    
    /**
     * 获取到的凭证
     */
    private String access_token;

    /**
     * 凭证有效时间,单位:秒
     */
    private Integer expires_in;

    /**
     * 时间验证
     *
     * @return
     */
    public boolean isAvailable() {
        if (StringUtils.isNotBlank(access_token) && this.expires_in > 1) {
            return true;
        }
        return false;
    }

    @Override
    public boolean matching() {
        return isAvailable();
    }
}

package com.jfinal.weixin.api.model.menu;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-18
 */

/**
 * 个性化菜单数据
 */
@Data
public class ConditionalMenu implements Serializable {
    /**
     * @Fields button : 一级菜单数组，个数应为1~3个
     */
    private MenuButton[] button;

    /**
     * 创建个性化自定义菜单规则
     */
    private MenuMatchrule matchrule;

    /**
     * 查询个性化菜单返回的menuid
     */
    private String menuid;
}

package com.jfinal.weixin.api;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.jfinal.utils.Exceptions;
import com.jfinal.utils.http.HttpUtil;
import com.jfinal.utils.RetryUtils;
import com.jfinal.weixin.api.model.AccessToken;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-15
 */

/**
 * 获取微信公众号全局唯一票据的API
 * 完全独立使用, 缓存方案由插件提供支持
 */
@Slf4j
public enum AccessTokenApi {
    /**
     * 使用枚举实现单例, 调用方式: AccessTokenAPI.ice.Method()
     */
    ice;

    /**
     * 获取微信公众号全局唯一票据
     * 使用了RetryUtils 工具进行三次重试
     *
     * @param appid  第三方用户唯一凭证
     * @param secret 第三方用户唯一凭证密钥,即appsecret
     * @return
     */
    public AccessToken getAccessToken(String appid, String secret) {
        String url = APIConfig.WX__ACCESS_TOKEN;
        url = url.replace("APPID", appid).replace("APPSECRET", secret);

        final String finalUrl = url;
        //失败时重试三次
        AccessToken accessToken = RetryUtils.retryOnException(3, new Callable<AccessToken>() {
            @Override
            public AccessToken call() throws Exception {
                ApiResult apiResult = ApiResult.create(HttpUtil.get(finalUrl));
                return JSON.parseObject(apiResult.getJson(), AccessToken.class);
            }
        });

        if (accessToken != null && accessToken.getAccess_token() != null && accessToken.getExpires_in() > 0) {
            return accessToken;
        }
        return null;
    }

    /**
     * 微信服务器IP地址列表
     *
     * @param accessToken 公众号的access_token
     * @return
     */
    public List<String> getWeixinCallbackIp(String accessToken) {
        String url = APIConfig.WX__CALLBACKIP;
        url = url.replace("ACCESS_TOKEN", accessToken);

        ApiResult apiResult = ApiResult.create(HttpUtil.get(url));

        List<String> ipList = new ArrayList<>();
        if (apiResult.isSuccessful()) {
            try {
                JSONArray array = JSON.parseObject(apiResult.getJson()).getJSONArray("ip_list");
                for (Object ipStr : array) {
                    ipList.add(ipStr.toString());
                }
            } catch (Exception e) {
                log.error("get weixin server ip list fail: {}", e);
                throw Exceptions.unchecked(e);
            }
        }
        return ipList;
    }


}

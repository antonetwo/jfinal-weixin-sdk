package com.jfinal.weixin.api;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-18
 */

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.utils.http.HttpUtil;
import com.jfinal.weixin.api.model.UserGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户分组管理
 */
public enum UserGroupApi {
    ice;

    /**
     * 创建一个用户组
     *
     * @param accessToken 公众号的access_token
     * @param groupName   用户组名称
     * @return
     */
    public ApiResult create(String accessToken, String groupName) {
        String url = APIConfig.WX__USER_GROUP_CREATE;
        url = url.replace("ACCESS_TOKEN", accessToken);

        Map<String, Map<String, String>> groupData = new HashMap<>();
        Map<String, String> mapData = new HashMap<>();
        mapData.put("name", groupName);
        groupData.put("group", mapData);

        return ApiResult.create(HttpUtil.post(url, JSON.toJSONString(groupData)));
    }

    /**
     * 查询所有用户组
     *
     * @param accessToken 公众号的access_token
     * @return
     */
    public List<UserGroup> getAll(String accessToken) {
        String url = APIConfig.WX__USER_GROUP_GET_ALL;
        url = url.replace("ACCESS_TOKEN", accessToken);

        List<UserGroup> groups = new ArrayList<>();

        ApiResult apiResult = ApiResult.create(HttpUtil.get(url));
        if (apiResult.isSuccessful()) {
            JSONObject groupJSON = JSONObject.parseObject(apiResult.getJson());
            if (groupJSON != null && !groupJSON.getJSONArray("groups").isEmpty()) {
                groups = JSON.parseArray(groupJSON.getString("groups"), UserGroup.class);
            }
        }
        return groups;
    }

    /**
     * 查询单个用户的所在分组
     *
     * @param accessToken 公众号的access_token
     * @param openId      用户的OpenId
     * @return
     */
    public ApiResult getId(String accessToken, String openId) {
        String url = APIConfig.WX__USER_GROUP_GET_ID;
        url = url.replace("ACCESS_TOKEN", accessToken);

        Map<String, String> groupData = new HashMap<>();
        groupData.put("openid", openId);

        return ApiResult.create(HttpUtil.post(url, JSON.toJSONString(groupData)));
    }

    /**
     * 修改用户分组名称
     *
     * @param accessToken  公众号的access_token
     * @param groupId      要修改的分组ID
     * @param groupNewName 要修改的新分组名称
     * @return
     */
    public ApiResult update(String accessToken, String groupId, String groupNewName) {
        String url = APIConfig.WX__USER_GROUP_UPDATE;
        url = url.replace("ACCESS_TOKEN", accessToken);

        Map<String, Map<String, String>> groupData = new HashMap<>();
        Map<String, String> mapData = new HashMap<>();
        mapData.put("id", groupId);
        mapData.put("name", groupNewName);
        groupData.put("group", mapData);

        return ApiResult.create(HttpUtil.post(url, JSON.toJSONString(groupData)));
    }

    /**
     * 移动用户分组
     *
     * @param accessToken 公众号的access_token
     * @param openId      用户唯一标识符OpenId
     * @param toGroupId   分组Id
     * @return ApiResult
     */
    public ApiResult membersUpdate(String accessToken, String openId, Integer toGroupId) {
        String url = APIConfig.WX__USER_GROUP_MOVE;
        url = url.replace("ACCESS_TOKEN", accessToken);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("openid", openId);
        mapData.put("to_groupid", toGroupId);

        return ApiResult.create(HttpUtil.post(url, JSON.toJSONString(mapData)));
    }

    /**
     * 批量移动分组
     *
     * @param accessToken 公众号的access_token
     * @param openIdList  openId集合
     * @param toGroupId   分组Id
     * @return
     */
    public ApiResult membersBatchUpdate(String accessToken, List<String> openIdList, Integer toGroupId) {
        String url = APIConfig.WX__USER_GROUP_MOVE_BATCH;
        url = url.replace("ACCESS_TOKEN", accessToken);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("openid_list", openIdList);
        mapData.put("to_groupid", toGroupId);

        return ApiResult.create(HttpUtil.post(url, JSON.toJSONString(mapData)));
    }

    /**
     * 删除分组
     *
     * @param accessToken 公众号的access_token
     * @param groupId     分组Id
     * @return
     */
    public ApiResult delete(String accessToken, Integer groupId) {
        String url = APIConfig.WX__USER_GROUP_DELETE;
        url = url.replace("ACCESS_TOKEN", accessToken);

        Map<String, Map<String, Object>> groupData = new HashMap<>();
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("id", groupId);
        groupData.put("group", mapData);

        return ApiResult.create(HttpUtil.post(url, JSON.toJSONString(groupData)));
    }

}

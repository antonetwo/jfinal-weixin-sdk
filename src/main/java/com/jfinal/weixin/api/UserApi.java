package com.jfinal.weixin.api;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-24
 */

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.utils.Exceptions;
import com.jfinal.utils.http.HttpUtil;
import com.jfinal.weixin.api.model.FollowList;
import com.jfinal.weixin.api.model.Follower;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户管理接口
 */
public enum UserApi {
    ice;

    /**
     * 设置备注名
     * 开发者可以通过该接口对指定用户设置备注名，该接口暂时开放给微信认证的服务号
     *
     * @param accessToken 公众号的access_token
     * @param openId      用户标识
     * @param remark      新的备注名，长度必须小于30字符
     * @return
     */
    public ApiResult updateRemark(String accessToken, String openId, String remark) {
        String url = APIConfig.WX__USER_UPDATE_REMARK;
        url = url.replace("ACCESS_TOKEN", accessToken);

        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("openid", openId);
        dataMap.put("remark", remark);

        return ApiResult.create(HttpUtil.post(url, JSON.toJSONString(dataMap)));
    }

    /**
     * 获取用户基本信息（包括UnionID机制）
     *
     * @param accessToken 公众号的access_token
     * @param openId      用户标识
     * @return
     */
    public ApiResult getFollowerInfo(String accessToken, String openId) {
        String url = APIConfig.WX__USER_GET_INFO;
        url = url.replace("ACCESS_TOKEN", accessToken);
        url = url.replace("OPENID", openId);

        return ApiResult.create(HttpUtil.get(url));
    }

    /**
     * 批量获取用户基本信息
     * 开发者可通过该接口来批量获取用户基本信息。最多支持一次拉取100条
     *
     * @param accessToken 公众号的access_token
     * @param openIdList  用户标识
     * @return
     */
    public ApiResult getFollowerList(String accessToken, List<String> openIdList) {
        String url = APIConfig.WX__USER_GET_INFO_BATCH;
        url = url.replace("ACCESS_TOKEN", accessToken);

        Map<String, List<Map<String, Object>>> userListMap = new HashMap<>();

        List<Map<String, Object>> userList = new ArrayList<>();
        for (String openId : openIdList) {
            Map<String, Object> mapData = new HashMap<>();
            mapData.put("openid", openId);
            mapData.put("lang", "zh_CN");
            userList.add(mapData);
        }

        userListMap.put("user_list", userList);
        return ApiResult.create(HttpUtil.post(url, JSON.toJSONString(userListMap)));
    }

    /**
     * 获取关注用户列表
     *
     * @param accessToken 公众号的access_token
     * @param nextOpenId  第一个拉取的OPENID，不填默认从头开始拉取
     * @return
     */
    public ApiResult getFollowerList(String accessToken, String nextOpenId) {
        String url = APIConfig.WX__USER_GET_OPENID_LIST;
        url = url.replace("ACCESS_TOKEN", accessToken);

        if (nextOpenId != null && StringUtils.isNotBlank(nextOpenId)) {
            url = url.replace("NEXT_OPENID", nextOpenId);
        } else {
            url = url.replace("NEXT_OPENID", "");
        }
        return ApiResult.create(HttpUtil.get(url));
    }

    /**
     * 转换json数据为UserInfo对象
     *
     * @param dataJson json数据
     * @return
     */
    public Follower convertFollower(String dataJson) {
        Follower userInfo = null;
        try {
            if (StringUtils.isNotBlank(dataJson)) {
                userInfo = JSON.parseObject(dataJson, Follower.class);
            }
        } catch (Exception e) {
            throw Exceptions.unchecked(e);
        }
        return userInfo;
    }

    /**
     * 转换json数据为UserInfo集合
     *
     * @param dataJsonList json数据
     * @return
     */
    public List<Follower> convertFollowerList(String dataJsonList) {
        List<Follower> userList = new ArrayList<>();
        try {
            if (StringUtils.isNotBlank(dataJsonList)) {
                JSONObject jsonObject = JSON.parseObject(dataJsonList);
                if (jsonObject != null && !jsonObject.isEmpty() && jsonObject.getJSONArray("user_info_list").size() > 0) {
                    userList = JSON.parseArray(jsonObject.getString("user_info_list"), Follower.class);
                }
            }
        } catch (Exception e) {
            throw Exceptions.unchecked(e);
        }
        return userList;
    }

    /**
     * 转换批量获取用户列表的Json数据为对象
     *
     * @param dataJson json数据
     * @return
     */
    public FollowList convertFollowListForOpenId(String dataJson) {
        FollowList followList;
        try {
            JSONObject jsonObject = JSON.parseObject(dataJson);
            followList = JSON.parseObject(dataJson, FollowList.class);
            if (jsonObject.getInteger("count") > 0 && !jsonObject.getJSONObject("data").isEmpty() && jsonObject.getJSONObject("data").getJSONArray("openid").size() > 0) {
                followList.setOpenIds(JSON.parseArray(jsonObject.getJSONObject("data").getString("openid"), String.class));
            }
        } catch (Exception e) {
            throw Exceptions.unchecked(e);
        }

        return followList;
    }


}

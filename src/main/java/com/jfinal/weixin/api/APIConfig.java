package com.jfinal.weixin.api;

/**
 * @author LarryKoo(larrykoo@126.com)
 * @date 2015-12-15
 */

import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;

/**
 * 微信API常量
 */
public class APIConfig {

    private static Prop apiProp = PropKit.use("wechat_api.properties");

    /**
     * ======================================================
     * ==================== 获取接口调用凭证 ==================
     * ======================================================
     */
    public final static String WX__ACCESS_TOKEN = apiProp.get("access_token");
    public final static String WX__CALLBACKIP = apiProp.get("callbackip");

    /**
     * ======================================================
     * ==================== 默认自定义菜单 ====================
     * ======================================================
     */
    public final static String WX__MENU_GET = apiProp.get("menu.get");
    public final static String WX__MENU_DELETE = apiProp.get("menu.delete");
    public final static String WX__MENU_CREATE = apiProp.get("menu.create");
    public final static String WX__MENU_GET_CURRENT_SELF_MENU_INFO = apiProp.get("menu.get_current_self_menu_info");

    /**
     * ======================================================
     * ==================== 个性化自定义菜单 ==================
     * ======================================================
     */
    public final static String WX__MENU_CREATE_CONDITIONAL = apiProp.get("menu.create_conditional");
    public final static String WX__MENU_DELETE_CONDITIONAL = apiProp.get("menu.delete_conditional");
    public final static String WX__MENU_TRYMATCH_CONDITIONAL = apiProp.get("menu.trymatch_conditional");


    /**
     * ======================================================
     * ==================== 发送消息 =========================
     * ======================================================
     */
    //    public final static String WX__ = apiProp.get("");


    /**
     * ======================================================
     * ==================== 用户分组管理 ======================
     * ======================================================
     */
    public final static String WX__USER_GROUP_CREATE = apiProp.get("user.group_create");
    public final static String WX__USER_GROUP_GET_ALL = apiProp.get("user.group_get_all");
    public final static String WX__USER_GROUP_GET_ID = apiProp.get("user.group_get_id");
    public final static String WX__USER_GROUP_UPDATE = apiProp.get("user.group_update");
    public final static String WX__USER_GROUP_MOVE = apiProp.get("user.group_move");
    public final static String WX__USER_GROUP_MOVE_BATCH = apiProp.get("user.group_move_batch");
    public final static String WX__USER_GROUP_DELETE = apiProp.get("user.group_delete");

    /**
     * ======================================================
     * ==================== 用户管理 =========================
     * ======================================================
     */
    public final static String WX__USER_UPDATE_REMARK = apiProp.get("user.updateremark");
    public final static String WX__USER_GET_INFO = apiProp.get("user.get_info");
    public final static String WX__USER_GET_INFO_BATCH = apiProp.get("user.get_info_batch");
    public final static String WX__USER_GET_OPENID_LIST = apiProp.get("user.get_openid_list");

    /**
     * ======================================================
     * ==================== 授权获取用户信息 ==================
     * ======================================================
     */
    public final static String WX__USER_OAUTH_URL = apiProp.get("user.oauth.url");
    public final static String WX__USER_OAUTH_AUTHORIZE = apiProp.get("user.oauth.access_token");
    public final static String WX__USER_OAUTH_AUTHORIZE_REFRESH = apiProp.get("user.oauth.refresh_token");
    public final static String WX__USER_OAUTH_USERINFO = apiProp.get("user.oauth.userinfo");

}

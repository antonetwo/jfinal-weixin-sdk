# 独立的微信公众平台SDK-Kit
### 站在巨人的肩膀上

### 开启SDK在线文档新篇章：http://weixin-sdk-guide.readthedocs.org/

#### 基于jfinal-weixin 诞生了 jfinal-weixin-sdk
---

> jfinal-weixin-sdk 是基于 JFinal-weixin 的微信公众号极速开发 SDK

> 不依赖Web的SDK Kit，支持搭建多帐号系统，可在任何环境下使用。

> jfinal-weixin-sdk是站在巨人的肩膀人, 诞生离不开 jfinal-weixin, mpsdk4j, ____′↘夏悸/wechat

### 环境依赖


> 使用了JDK 1.7

> 标准的Maven项目 （暂未发布到中央仓库,可导入源代码后引用依赖或自行使用mvc install编译到本地仓库）

```xml
<dependency>
    <groupId>com.jfinal</groupId>
    <artifactId>jfinal-weixin-sdk</artifactId>
    <version>1.0.0</version>
</dependency>
```

> 使用了lombok来的自动生成Getter & Setter,需要IDE安装lombok插件,并引入Maven [https://projectlombok.org]

```xml
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.16.6</version>
</dependency>
```

### 开始使用 

#### `如不使用JFinal构建Web项目,也可完全独立使用,可参考源码中mvc包里面jfinal包,使用其他Servlet技术重写其中的两个类即可`

> 第一步. 在一个JFinal Web项目中创建一个新的Controller,继承与`BasicWeChatMessageController`

```java
public class NewWeChatController extends BasicWeChatMessageController {
    /**
     * 查询获取WeixinConfig对象
     *
     * @param id 通过该id,查询对应的公众帐号信息,该id由下一步填入公众平台的`URL/id` 传入
     * @return
     */
    @Override
    public WeixinConfig getWeChatConfig(String id) {
    
        //通过该id,查询对应的公众帐号信息,返回一个WeixinConfig对象
        
        WeixinConfig config = new WeixinConfig("token", "appid", "appSecret");
        config.setEncodingAesKey("encodingAesKey");
        
        /*
         * 使用自定义的人消息处理驱动,继承`MessageProcessingHandler` 实现其中接口, 将对象传入config
         * 注意,继承后需要添加对象 `private OutMsg outMessage;` 实现Getter & Setter,可参考 `DefaultMessageProcessingHandler`
         *
         * 为空时使用默认消息处理器`DefaultMessageProcessingHandler`
         */
        config.setMessageHandler(null);
        return config;
    }
}
```

> 第二步. 在微信公众平台后台中填入URL

```
// 建议分配一个一级目录给微信相关请求使用, 如`http://xxx.com/wechat/`
// `/message` 为驱动请求入口地址
// `id` 为支持多帐号的帐号id, 填入id与传入的WeixinConfig对象中的`id`一致, 可通过该id从数据库中查询出对应的微信帐号信息

URL : http://xxx.com/xxx/message/id
Token: token 

```

### 完善中。。。。。